all: clean build

clean:
	rm -f output.zip || true
build:
	chmod +x -R META-INF
	chmod +r -R system
	zip -r output.zip META-INF system
flash:
	adb sideload output.zip
